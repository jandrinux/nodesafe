console.log("Socket Safecard");
var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');
var needle = require('needle');
var PORT = 82;
var ROOMS = [];
var usernames = []; 
var USERIP = [];
var DEBUG = true;



app.use(express.static(__dirname + '/public'));

var mysql = require('mysql');
var connection = mysql.createConnection({
   host: 'localhost',
   user: 'root',
   password: 'JMiRh98knDt5RkBmHN69',
   database: 'clon_safecard',
   port: 3306
});

connection.connect(function(error){
   if(error){
      console.log(error);
   }else{
      console.log('Conexion correcta.');
   }
});

io.sockets.on('connection', function(socket){
	


	socket.on('client-condo', function(username){
		var ip= socket.request.connection.remoteAddress.replace('::ffff:','');

		USERIP.push({condo: username,ip: ip });
		console.log(USERIP);

		socket.username = username;
		if(!find_un(username)){
			usernames.push(username);
		}
		socket.join(username);
		 
		if (DEBUG) 
			console.log("SOCKET->CLIENT-CONDO:" + username)
		
		query(username);
	}); 
	socket.on('manuals-condo', function(username){
		var ip= socket.request.connection.remoteAddress.replace('::ffff:','');
 
		USERIP.push({condo: username,ip: ip });
		console.log(USERIP);

		socket.username = username;
		if(!find_un(username)){
			usernames.push(username);
		}
		socket.join(username);
		 
		if (DEBUG) 
			console.log("SOCKET->MANUALS-CONDO:" + username)
		manuals(username.replace('m',''));
	}); 

	socket.on("client-barrier-id",function(data){
		console.log("Client-Barrier-ID: " + data.id + " Condo: " + data.condo)
		var querys = connection.query("update  barriers set processed=1 where id=" + data.id, function(error,result){	
				if (DEBUG)
						console.log("SOCKET->Client-Barrier-ID (CALLBACK): CONDO: " + data.condo + " ID_BARRIER: " + data.id);

				query(data.condo);
		})
	});
	socket.on("client-manuals-ids",function(data){
		console.log("Client-manuals-IDs:" + data);

		for (var i = 0;i< data.length;i++) 
		{
			 var id = data[i];
			 var sql = "update manuals set sent_gate=1 where id=" + id;	 
			 console.log(sql);
			 var qs = connection.query(sql,function(error,result){
			 	console.log("SOCKET->Client-manuals-IDs OK");
			 });
		};
	});

	function find_un(id){
		for (var i = 0 ; i < usernames.length; i++) {
			if (id==usernames[i]) {
				return true;
			}
		}
		return false;
	}


	socket.on('disconnect', function(socket) {
		 console.log('Client DISCONECT');

	});

	//ENQUIQUE LOGS TO SEVER BACKEND
	socket.on('client-log',function(data,callback){
		console.log("LOGS TO SERVER.. " + data)
		var q = JSON.parse(data);
		var retorno = [];
		var url = "http://lab.safecard.cl/soap/ws/logCondo/";
		var url2 = "http://lab.safecard.cl/soap/ws/LogService/";
		for (var i = 0; i<q.length; i++)
		{
			if (q[i].WebService==0) {
						console.log("WebService == 2")
						var p = {
						  code: q[i].code,
						  regdate: q[i].regdate,
						  type : q[i].type
						};
					      
						console.log(url);
						needle.post(url,p,function(error, res){
							console.log(res.body);
						});
			 }
			 if (q[i].WebService==2) {
			 	 //$condo_id, $service_id, $house_name, $guard_mobile, $type

			 	 var p = {
			 	 		condo_id: q[i].condo_id,
			 	 		service_id: q[i].Service_id,
			 	 		house_name: q[i].House_Name,
			 	 		guard_mobile: q[i].Mobile_Guard ,
			 	 		type: q[i].type
			 	 }
			 	 console.log(url2);
						needle.post(url2,p,function(error, res){
							console.log(res.body);
				  });

			 }



			retorno.push(q[i].id);
		};

		return callback(retorno);	
	});
	//MANUALS



	app.get('/q/:Id',function(req,res){
		if (DEBUG)
			console.log("Q =>BACKEND TO SOCKET CONDO_ID: " + req.params.Id);
		query(req.params.Id);
		res.send('condo_id:' + req.params.Id);
	});

	app.get('/manuals/:Id',function(req,res){
		if(find_un(req.params.Id)) {
				if (DEBUG) console.log("Manuals=>BACKEND TO SOCKET CONDO_ID: " + req.params.Id);
				manuals(req.params.Id);		
	 	}
	 	res.send('condo_id:' + req.params.Id);
	});

	function manuals(condo_id){
		var sql = "select * from manuals where condo_id=" + condo_id + " and sent_gate=0";
		var q = connection.query(sql,function(error,result){
				console.log("RESULT COUNT : " + result.length);
				if (result.length == 0) console.log(sql);
				if (result.length>0) {

		   	for (var i =0; i<result.length;i++) {
      		result[i].name_organizer = 	result[i].name_organizer.replace(/[^a-zA-Z0-9]/g,' ');;
      		result[i].name_guest = 	result[i].name_guest.replace(/[^a-zA-Z0-9]/g,' ');;
      		console.log(result[i].name_organizer);
      	}
      	console.log("manuals send ");
			  io.sockets.in('m'+ condo_id).emit('client-manuals',result);
			}
		});
	}

	function query (data) {
		var query = connection.query('select aes,type,id,condo_id, code from barriers where condo_id=' + data  +' and processed = 0 and result="ACK" and active=1 limit 0,1', function(error, result){
		  if(!error){
		  	  if (DEBUG) {
		  	  	console.log("QUERY CONDO: " + data + " RESULT Count: " + result.length + " Result SQL: ");
		  	  	console.log(result);
			      	  
	         	}
		        if (result.length >0 ) 
		    	 		io.sockets.in(data).emit("client-barriers",result);

		    }
		 }
		);
	}
});


app.get('/',function(req,res){
  res.render('/public/index.html');
})
http.listen(PORT, function(){
  console.log('listening on *:' +PORT);
});

